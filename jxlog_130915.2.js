/*

jxlog.js (130803.1)
by Runsun Pan

A javascript logging tool for layered display of output blocks in the console.


Demo
====

http://jsfiddle.net/runsun/W4Mfs/10/


Usage
=====

    var l=jxlog;
    
    l('start test')     // jxlog() along works like console.log()
    function ff(a){
        l.b()           // .b(): begin a block. Ii will get function
                        // name automatically if no arg given
        n=3
        l('start with n=', 3)               // normal log line
            l.b('while')    // Start another block with given arg
            while(n--){
               l('n=', n)   // normal log line   
                   l.b('while n=',n)                // another block
                   l('test array display:', [n,1])  // test object arg
                   l('nnn')   
                   l.e()                            // ending  a block 
            }     
            l.e() 
        l.e()
     l.e()   
    }

    ff(3)

Result (in console) 
===================

    start test 
    .================ ff() 
    |  start with n= 3 
    |  ################# while 
    |  #  n= 2 jxlog.js:31
    |  #  .---------------- while n= 2 
    |  #  :  test array display: [2, 1] 
    |  #  :  nnn jxlog.js:31
    |  #  `---------------- while n= 2 
    |  #  n= 1 jxlog.js:31
    |  #  .---------------- while n= 1 
    |  #  :  test array display: [1, 1] 
    |  #  :  nnn jxlog.js:31
    |  #  `---------------- while n= 1 
    |  #  n= 0 jxlog.js:31
    |  #  .---------------- while n= 0 
    |  #  :  test array display: [0, 1] 
    |  #  :  nnn jxlog.js:31
    |  #  `---------------- while n= 0 
    |  ################# while 
    `================ ff()     

Ref: 
   
-- Chrome: https://developers.google.com/chrome-developer-tools/docs/console-api
--         https://developers.google.com/chrome-developer-tools/docs/commandline-api
-- Firefox: http://getfirebug.com/logging    


    // =====
    // Usage
    // =====
    //
    //    jxlog('Clearing data')
    //    jxlog.b('function xxx starts')  // starts a block 
    //    jxlog.b()   // will try to seek function name automatically
    //    jxlog.e()   // ends a block 
    //
    //    jxlog('test array display:', [n,1], ' obj:', {a:3})
    
    // ==============
    // Customization 
    // ==============
    //
    // jxlog.indent= '  ';
    // jxlog.init_string=[...] show version info at start. Set to false or [] to disable it. 
    // jxlog.mode       = 0;     // 0=default BODE_BOX, 1=jxlog.MODE_GROUP that use console.group
    // jxlog.border_colors=['green','darkred','blue','black', 'purple']; // border color choices
    // jxlog.bg_color_digits='9abcdef' // The range where the background color is built on 
    // jxlog.show_func_args=false;     // if true, function block will starts with 
    //                                 // funcname( arguments ) in stead of funcname()
    // jxlog.block_beg = '-';  // symbol for block beginning                          
    // jxlog.block_end = '-';  // symbol for block ending                          
    
    // =====
    // Basic
    // =====
    //
    // For jxlog(x,y,z), the arguments [x,y,z] will be converted to:
    //  
    // [ prefix, css_strings, x,y,z] where
    //
    //   prefix = "%c %c %c " 
    // 
    //      The number of %c =layer count, and 
    //            size of space betwee %c = jxlog.indent
    //
    //   css_string = multiple strings each is a string representation of css  
    //
    // Example:
    //
    //   jxlog( x, y, z ) in the layer-2:
    //
    //   console.log( "%c %c ", "color:red", "color:blue;background:yellow", x, y, z )
    //
    // But there's more. If x,y,z are: "var b=", b, "in xxx"
    // they will be appended to the first item of arguments:
    //
    //   console.log( "%c %c var b= %O in xxx",
    //               "color:red", "color:blue;background:yellow")
    //
    // Such design allows the last %c style to cover all the way to "var b=" and "in xxx"
    //
    // The %O format symbol allows a non-string argument to be shown as evalulated object.
    //     
    // jxlog.init_string: show version info in the beginning. Set to false or [] to disable it. 



*/

//
// To debug this file. Unmment the following line,
// and replace all "//log_(" to "log_("
//
//
function log_(){ console.log.apply(console, [].slice.apply(arguments) ) }
//
//log_( 'Entered jxlog.js' );

JXLOG_BLOCK_BEG= ':'; // symbol for block beginning
JXLOG_BLOCK_END= ':'; // symbol for block ending 

JXLOG_INDENT   = '  ';
JXLOG_SHOW_FUNC_ARGS= false;  // Show function arguments when using jxlog.b()

// MODE_BOX-specific 
JXLOG_MODE_BOX_BG_CHARS    ='bcdef'; // The range where the background color is built on ;    
JXLOG_MODE_BOX_BORDER_COLORS= ['green','darkred','blue','black', 'purple']; // border color random choices; 

// MODE_LINE-specific
JXLOG_MODE_LINE_SYMBOLS= ['++|=', 'oo|-', "++|=",'oo|*']; // "top-left, bot-left, vline, line"



(function()
{    

    
    function jxlog()
    {
        var jl = arguments.callee;
        var layer= jl.blocks.length;
        
        if( typeof arguments[0]=== 'boolean' ){ 
            jl.on= arguments[0] } // jxlog(true); jxlog(false)
        
        if(!jl.on) return 
        
        var _init_string;
        if( jl.init_string ){
            if( jl.init_string==true )( 
                _init_string  = ['\n%c             jxlog.js(ver.'+jxlog.version
                                +') reporting             \n'
                                , "background:khaki;border:1px solid;color:black"]    
            )    
            if( jl.init_string.length>0){
                  console.log.apply( console,jl.init_string ) 
                  jl.init_string = false ;
            }      
        }
        //log_('%c-------- Entered jxlog(), blockstate="%s"'
        //           ,"background:yellow", jl.blockstate, ', prefixes = ', jl.prefixes
        //           ,'args = ', arguments  );
                   
        // Make a copy of arguments            
        var args = [].slice.apply( arguments ); 
                
        switch (jl.mode)
        {
        
            case jl.MODE_GROUP: 
            
                 console.log.apply( console, args );
                 break; 

                 
            case jl.MODE_LINE: 
                
                //jl.prefixes.push( this.indent );
                
                var ps  = [].concat( jl.prefixes );
                //var css = [].concat( jl.blockcss );
                
                /*if( ps.length>0 ) // ps : ['%c ', '%c ' ...]
                {
                    switch (jl.blockstate)
                    {
                    //case 'beg':
                    //    ps= ps.slice(0,-1) ; 
                        
                    //    break;
                        
                    //case 'end':
                        ps= ps.slice(0,-1) 
                        break; 
                    }                         
                } 
                 */
                ps = [ ps.join('')+(layer==0?'':jl.indent) ] //.concat( css );
                
                var newargs=[];
                //args.push( jl.blockstate=='beg'
                //           ?'                              ' 
                //           :jl.indent 
                 //        )
                          
                // Hard parts: loop args, if item x is a string, pop and attech 
                // it to ps[0]; if it is an object, attach a "%0" to ps[0] but 
                // keep the object 
                var n =args.length;
                while(n--)
                {
                    x=args.shift() ;
                    if(typeof x ==='string')
                    {
                        ps[0]+= x 
                    } else {
                        if( !!(x && x.nodeType === 1) ) // DOM element 
                        { ps[0] += '%o' }
                        else if( typeof x ==='number'){
                           ps[0] += parseInt(x)==x? '%i':'%f' 
                        }
                        else if( typeof x ==='object' ) { ps[0]+= '%O' }
                        else { ps[0]+= '%s'}
                        newargs.push(x);
                    }
                }
                console.log.apply( console, ps.concat(newargs) );
                
                break; 
                
            default: // MODE_BOX
                
                var ps  = [].concat( jl.prefixes );
                var css = [].concat( jl.blockcss );
                
                if( ps.length>0 ) // ps : ['%c ', '%c ' ...]
                {
                    switch (jl.blockstate)
                    {
                    case 'beg':
                        ps= ps.slice(0,-1).concat(['%c'+jl.block_beg])
                        css[css.length-1]+=';font-weight:900;border-top:1px solid '
                        + jl.bordercolor + ';padding-top:2px;margin-top:3px;color:'+jl.bordercolor;
                        
                        break;
                        
                    case 'end':
                        ps= ps.slice(0,-1).concat(['%c'+jl.block_end])
                        css[css.length-1]+=';border-bottom:1px solid '
                        + jl.bordercolor + ';padding-bottom:2px;margin-bottom:3px';
                        break; 
                    }                         
                }                            
                
                ps = [ ps.join('') ].concat( css );
                
                var newargs=[];
                args.push( jl.blockstate=='beg'
                           ?'                              ' 
                           :jl.indent 
                         )
                // Hard parts: loop args, if item x is a string, pop and attech 
                // it to ps[0]; if it is an object, attach a "%0" to ps[0] but 
                // keep the object 
                var n =args.length;
                while(n--)
                {
                    x=args.shift() ;
                    if(typeof x ==='string')
                    {
                        ps[0]+= x 
                    } else {
                        if( !!(x && x.nodeType === 1) ) // DOM element 
                        { ps[0] += '%o' }
                        else if( typeof x ==='number'){
                           ps[0] += parseInt(x)==x? '%i':'%f' 
                        }
                        else if( typeof x ==='object' ) { ps[0]+= '%O' }
                        else { ps[0]+= '%s'}
                        newargs.push(x);
                    }
                }
                console.log.apply( console, ps.concat(newargs) );
                
        }
        
        jl.blockstate='';
    }
    jxlog.__history__ ={
     '130914.2':['All bugs fixed. jlog now is as good as jxlog.']
    ,'130914.1':['MODE_GROUP done. Fixing MODE_BOX: box top/bottom line didn\'t show.'
                 ,'http://jsfiddle.net/runsun/W4Mfs/15/'     ]
    ,'130913.1':['Restructuring: new design: jlog. MODE_LINE done.'
                 ,'http://jsfiddle.net/runsun/W4Mfs/14/'     ]
    ,'130911.1':['MODE_LINE done by patching up. Need code cleanup. TO DO: restructuring'
                 ,'http://jsfiddle.net/runsun/W4Mfs/14/'     ]
    ,'130910.1':['Add new mode: MODE_LINE (need fixes on log.e)'  ]
    ,'130806.1':['Set jxlog.on=true/false, or jxlog(true/false) to enable/disable logging.'  ]
    ,'130805.3':['Use jxlog.blockbgs to keep blocks of same layer the samg bg. '  ]
    ,'130805.2':['Add new options: block_beg, block_end'
               , 'Allow other formatting symbol: %o, %i, %f, %s besides %i and %O'  ]
    ,'130805.1':['Allows only MODE_BOX and MODE_GROUP'
               , 'Make MODE_BOX the default.',  
               , 'Add new option show_func_args, init_string.']
    ,'130804.2':['Redesign large part to allow MODE_BOX, which works nicely.']
    ,'130804.1':['Use .prefixes (an array) in place of _getprefix()'
               ,'Allow setting modes: jxlog.MODE=0 (default) is the plain mode'
               +', jxlog.MODE=jxlog.MODE_GROUP uses the console group()']
    ,'130803.1':['first draft']
    }
    
    jxlog._randombg=function(){   // random background color 
        var s='#',i=6;
        while(i--){
            s+= this.bg_color_digits.charAt( parseInt(Math.random()*this.bg_color_digits.length) )
        }
        return s
    }
    
    jxlog._randombordercolor=function(){
        var c=this.border_colors;
        return c[ parseInt(Math.random()*c.length) ]
    }
    
    jxlog._repeat= function(str, n){ var s ='';
        //log_('In _repeat, str=', str)
        for( var i=0;i<n;i++){ s+=str;} 
        //log_('leaving _repeat, s=', s)
        return s;
    }
    
    jxlog.on = true;
    
    jxlog.mode       = 0;  
    jxlog.MODE_BOX   = 0; // 0 w/o using console.group()                                
    jxlog.MODE_GROUP = 1; // 1 use console.group()                              
    jxlog.MODE_LINE  = 2; // 2 use ascii line 
    
    jxlog.bg_color_digits= 'bcdef'; // The range where the background color is built on ;    
    jxlog.border_colors= ['green','darkred','blue','black', 'purple']; // border color choices;  
    
    jxlog.block_beg = '>';  // symbol for block beginning                          
    jxlog.block_end = '<';  // symbol for block ending                          
    
    jxlog.block_leftborders='||'; 
    jxlog.block_hborders = ['++=', 'oo-', "++=",'++*','oo*']; // "tbh": t=top-corner, b=bot-corner, h: horizontal line
    
    jxlog.indent 	 = '  '  ;
    jxlog.show_func_args= false;  // Show function arguments when using jxlog.b()

    jxlog.version = Object.keys(jxlog.__history__)[0];
    //jxlog.init_string= ['\n%c             jxlog.js(ver.'+jxlog.version
    //                    +') reporting             \n'
    //                  , "background:khaki;border:1px solid;color:black" ]; 
    jxlog.init_string= true; 

    //                  
    // Don't modify the following:
    //
    jxlog.blockstate='';  // = ''|'beg'|'end'
    jxlog.blocks    =[];  // array of block labels
    jxlog.blockcss  =[];  // [ "color:red","color:blue;font-size:20px" ]
    jxlog.prefixes  =[];  // [ "%c ", "%c ", ... ] 
    jxlog.blockbgs  =[];  // background colors of each layer. They are generated by 
                          // _randombg in jxlog.b(), and, once generated, WILL NOT 
                          // be popped out like blocks, blockcss and prefixes when going 
                          // up one layer
       
    //#####################################
    //
    //  This section is for the new MODE_LINE (under construction)
    //
    
    jxlog.Block= function( label ){
        this.layer = jxlog.blocks.length;
        this.prefix   = ''; // "", "|  ", "|  !  " ... depends on layer
        this.beglabel = label;
        this.endlabel = ''; 
        this.css      = '';
        this.gethead= function(){
            return this.prefix
        }    
        this.getline= function(corner){ 
            // Return a horizontal line for the block (including prefix)
            // For mode=MODELINE
            // corner=0 for beg, 1 for end
               
            //log_( '_getblockline, layer=', layer, 'corner=', corner)
            // => "+=================="
            var symbols= jxlog.block_hborders;
            
            // symbols: ex: ".`=": .:top-corner, `:bot-corner, =: horizontal line
            symbols= symbols[ this.layer%symbols.length];  
            
            if( typeof corner==='undefined' ){ corner=0 }
            
            var line= jxlog._repeat( symbols.charAt(2), 30, corner);
            //log_('line = '+ line, ' symbols=', symbols );
            return this.prefix + symbols.charAt( corner ) + line           
        }
        
        this.prefix = jxlog.prefixes.join('');
        
        this.getsymbols= function(){ 
                            var layer  = jxlog.blocks.length;
                            var symbols= jxlog.block_hborders;
                            // symbols: ex: ".`=": .:top-corner, `:bot-corner, =: horizontal line
                            return symbols[ layer%symbols.length]; 
                        }    
                     
        this.begline = this.getline(0);
        this.endline = this.getline(1);
        
    }
    
    jxlog._getblockline= function(layer, corner){ // For mode=MODELINE
                                                  // corner=0 for beg, 1 for end
           
        //log_( '_getblockline, layer=', layer, 'corner=', corner)
        // => "+=================="
        if( typeof corner==='undefined' ){ corner=0 }
        var symbols= this.block_hborders;
        
        symbols= symbols[ layer%symbols.length];
        // symbols: ex: "++=": t=top-corner, b=bot-corner, h: horizontal line
        var line= this._repeat( symbols.charAt(2), 50, corner);
        //log_('line = '+ line, ' symbols=', symbols );
        return symbols.charAt( corner ) + line           
    }
    
    jxlog.addblock= function(label){
        this.blocks.push( new this.Block( label ) );
    }
    
    jxlog.popblock= function(){ // return: {label,}
        return this.blocks.pop()
    }
    
    //
    // 
    //  
    //#######################################
    
    jxlog.b= function(){
    
        if( typeof arguments[0] === 'boolean' ){ this.on = arguments[0] }
        if(!this.on) return 
        
        this.blockstate='beg';
        this.bordercolor = this._randombordercolor();
        var args= [].slice.apply(arguments);
        
        if(args.length==0){
            if( this.show_func_args ){
              args= [ arguments.callee.caller.name+'(', arguments.callee.caller.arguments, ')' ];
            } else {
              args= [ arguments.callee.caller.name+'()' ];
            }
        }
        
        switch (this.mode) 
        {
            case this.MODE_GROUP: 
                //args[0]= (!this.mode?'':(this._getblockbeg()+ this._getblockline()))
                //         +args[0];
                 
                //var prefix= this.BLOCK_SYMS.charAt(this.blocks.length) + this.indent; 
                //this.prefixes.push( prefix  )
                console.group.apply( console, args );
                break; 
                
            case this.MODE_LINE:
              
                // prefixes is: ['%c ', '%c ' ...]
                var layer= this.blocks.length;
                var leftborder = this.block_leftborders.charAt( 
                                  layer%this.block_leftborders.length ); 
                
                //this.apply( null, [ (layer==0?'':this.indent)+this._getblockline(layer)] );
                
                this.apply( null, [ this._getblockline(layer)] );
                
                args[0]= leftborder+this.block_beg+args[0];
                this.apply(null, args);
                this.prefixes.push( (layer==0?'':this.indent)+leftborder );
                
                break;
         
            default: // MODE_BOX
            
                // prefixes is: ['%c ', '%c ' ...]
                var layer= this.blocks.length;
                
                this.prefixes.push( '%c'+this.indent )
                if( this.blockbgs.length>= this.prefixes.length )
                {
                    var bg =this.blockbgs[ this.prefixes.length-1 ]
                }else{    
                    var bg= this._randombg();
                    this.blockbgs.push( bg );
                }
                // The css format for each '%c'. 
                this.blockcss.push( 'border-left:1px solid '
                        + this.bordercolor + '; background:'+ bg);
                        
                this.apply(null, args);
                
        } 
        this.blocks.push( args ); 
        
    }
     
    jxlog.e= function(){
        if( typeof arguments[0] === 'boolean' ){ this.on = arguments[0] }
        if(!this.on) return 
        
        //log_('%c\n--->> Entered .e(), jl.prefixes= ',"color:green", this.prefixes)
                this.blockstate='end'
        //var end = this._getblockend(1);        
        var args= [].slice.apply(arguments);        
        //this.apply(null, args); 
        switch (this.mode)
        {
            case this.MODE_GROUP:
            
                /*if(args.length==0){
                    args= this.blocks[ this.blocks.length-1] 
                    if(args && args[0]){
                    args[0]= end+args[0].slice(end.length);
                    }
                }   
                */
                this.blocks.pop();           
                this.prefixes.pop();
                this.blockcss.pop(); 
                console.groupEnd.apply( console, args );
                break; 
                
            case this.MODE_LINE:
            
                var tmp = this.blocks.pop()
                if(tmp){
                    //log_('tmp',tmp)
                    if( args.length==0){ args= tmp }
                    //args[0]= args[0].replace((new RegExp('^'+this.block_beg)),''); 
                    args[0]= args[0].slice(1+this.block_beg.length); 
                    
                    var layer= this.blocks.length;
                    //var tmp = this.blocks.pop()
                    if(args.length==0){ args= tmp; }
                    var leftborder = this.block_leftborders.charAt( 
                                      layer%this.block_leftborders.length );
                    
                    this.prefixes.pop();
                    
                    args[0]= leftborder+this.block_end+args[0];
                    //log_('args[0] = ', args[0]);
                    this.apply(null, args);      
                     
                    this.apply( null, [ this._getblockline(layer,1)] );
                }
                break;
                //this.blockcss.pop();            
                  
                
                //log_('%c---Leaving .e(), jl.prefixes= ',"color:green", this.prefixes, '\n')
                
            default: //case this.MODE_BOX:
            
                var tmp = this.blocks.pop()
                if(args.length==0){ args= tmp; }
                //log_('%c\n---in .e(), jl.prefixes= ',"color:green", this.prefixes)
                this.apply(null, args); 
                //log_('%c---Leaving .e(), jl.prefixes= ',"color:green", this.prefixes, '\n')
                this.prefixes.pop();
                this.blockcss.pop();            

        }  
        //log_('%c\n---Leaving .e(), jl.prefixes= ',"color:green", this.prefixes)
                
        
    }//

    window.jxlog= jxlog;
    
})()

function jlog(){

    //jxlog.b('Enter jlog, arguments=', jx.inspect(arguments))
    
    var jl = jlog;
    var args= [].slice.apply( arguments );
    var layer = jl.blocks.length;
    
    if(layer==0){
    
        console.log.apply( console, args );
        
    } else {
    
        //log_( 'Entered jlog, mode=', jl.mode, ', args =', arguments, ', jl.blocks=', jx.inspect(jl.blocks) );
        
        var prefix; 
        var blocks = jl.blocks;
        var block = blocks[ blocks.length-1 ]; 
                    // block is defined differently in dif. modes;
                    // in MODE_LINE: {symbol_vline, symbol_line, symbol_corner0, symbol_corner1} 
                    // in MODE_BOX : {style}
                    // in MODE_GROUP: {}
            
        var layer  = blocks.length;
        
        if( jlog.mode == jl.MODE_LINE ){
        
            if( jl.isbeg ){
                block.labels = [].concat(args);
                console.log( jl.getprefix(layer-1)+ block.symbol_corner0 + block.symbol_line );        
                args[0]= jl.getprefix(layer-1)  + block.symbol_vline+ block.index+jl.block_beg + args[0];
                console.log.apply( console, args ) 
            }
            
            else if( jl.isend ){
                if(typeof args==='undefined'||args.length==0) args = block.labels; 
                args[0]= jl.getprefix(layer-1) + block.symbol_vline+ block.index+jl.block_end + args[0];
                console.log.apply( console, args ) 
                console.log( jl.getprefix(layer-1)+ block.symbol_corner1 + block.symbol_line ); 
            } 
            
            else {
                args[0] = jl.getprefix()+ args[0] 
                console.log.apply( console, args)
            }  
        } 
        
        
        else if( jlog.mode == jl.MODE_GROUP ){                   // MOD_GROUP
            
            if( jl.isbeg ){ 
                args[0] = block.index+ jl.block_beg+ args[0];
                console.group.apply( console, args );
            }
            else if( jl.isend ){ 
                //console.groupEnd.apply( console, args )
                //console.log('---------------------------------');
                console.groupEnd();
            }  
            else { console.log.apply( console, args )
            }  
        
        } 
        
        else {                                         // MODE_BOX
            //
            // This case won't work:
            // console.log( "%c  %c  ", 'background:yellow', 'background:green', 'test')
            // ==> the green background won't extend to "test"
            //
            // and this won't work, either:
            // console.log( '%c  %c  ', 'background:yellow', 'background:green', '%ctest', //                         'background:red')
            // The last part shown as: "   %ctest background:red"
            // ==> the style symbol "%c" won't work unless it's in the first argument.  
            //
            // The only way to work is:
            // console.log( '%c  %c  test', 'background:yellow', 'background:green')
            // 
            // That is, we have to attach ALL args to the first arguments. 
            // 
            
            //log_( 'in jlog(), MODE_BOX' );
            
                
            var prefix = jl.getprefix()||[] //==> ['%c %c', style0, style1]
            
            //log_( 'In jlog(), MODE_BOX, block= ', block, ', prefix=', prefix )
            
            var style = block.styles? block.styles[ block.styles.length-1 ]: '';
            
            if( jl.isbeg ){
            
                // For the prefix= ['%c %c', style0, style1], the last 
                // style should cover the left and top borders
                prefix[0] = prefix[0].replace(/\s*$/,'')+ block.index + jl.block_beg; 
                prefix[prefix.length-1] += ';font-weight:900'
                                          +';border-top-width: 1px'
                                          +';padding-top:2px;margin-top:3px'
                                          +'; color: black'; 

                //log_('In jlog(), .isbeg, prefix =', jx.inspect( prefix ) )
                
                // Make the block-beg line longer                  
                args.push( '                        '); 
                
            } else 
            
            if( jl.isend ){

                // For the prefix= ['%c %c', style0, style1], the last 
                // style should cover the left and BOTTOM borders
                
                //log_( 'In jlog().isend ,args = ', jx.inspect(args), ', block = ', block )
                if(typeof args==='undefined'||args.length==0) args = block.labels; 
                //log_( 'In jlog().isend ,args = ', jx.inspect(args), ', block = ', block )
                prefix[0] = prefix[0].replace(/\s*$/,'') +block.index+jl.block_end; 
                
                prefix[prefix.length-1] += ';border-bottom-width:1px'
                                          +';padding-bottom:2px;margin-bottom:3px'
                                          +'; color:black';
            }

                
            // Hard parts: 
            // attaching all args to the first arguments sent to console.log.apply.
            // loop args, if item x is a string, pop and attech 
            // it to prefix[0]; if it is an object, attach a "%0" to ps[0] but 
            // keep the object 
            var newargs=[], n =args.length;
            if( jl.isbeg ) block.labels = [].concat( args );
            
            while(n--)
            {
                x=args.shift() ;
                if(typeof x ==='string')
                {
                    prefix[0]+= x 
                    
                } else {
                
                    if( !!(x && x.nodeType === 1) ) // DOM element 
                    { prefix[0] += '%o' }
                    else if( typeof x ==='number'){
                       prefix[0] += parseInt(x)==x? '%i':'%f' 
                    }
                    else if( typeof x ==='object' ) { prefix[0]+= '%O' }
                    else { prefix[0]+= '%s'}
                    newargs.push(x);
                }
            }
            //log_(' blockstyles = ', jx.inspect( jl.blockstyles ) );
            console.log.apply( console, prefix.concat(newargs) );
        }  
    } // layer>0    
    //jxlog.e()
} // jlog()

jlog.mode      = 0;
jlog.MODE_BOX  = 0;
jlog.MODE_LINE = 1;
jlog.MODE_GROUP= 2;

jlog.isbeg     = false;
jlog.isend     = false; 
jlog.blocks    = [];
jlog.blockstyles=[];  // set background and border-left color. The length of this var
                      // will not be reduced when the layer is reduced, so when the same
                      // layer of block is added the next time, new block has the same 
                      // style as that of the previous same-layer block;

jlog.subindex = 0;  // for the indices of first layer blocks. Each block has an .index
                    //  var that increases when blocks on the same layer increase.
                    //  It is controlled by a ".subindex" var of their parent block
                    //  So each block now will have two new vars: .subindex, .index
                    //  We also throw in a .parent for easier op for parent.subindex
                    //  If it's a top-layer (layer==0), parent = jlog. So the top
                    //  layer parent, jlog, needs a .subindex. This feature is implented
                    //  in the addnewblock() funciton. 
                      
jlog.indent    = JXLOG_INDENT;
jlog.show_func_args= JXLOG_SHOW_FUNC_ARGS;  // Show function arguments when using jxlog.b()
jlog.block_beg = JXLOG_BLOCK_BEG;  // symbol for block beginning                          
jlog.block_end = JXLOG_BLOCK_END;  // symbol for block ending                          

// MODE_LINE-specific
jlog.mode_line_symbols = JXLOG_MODE_LINE_SYMBOLS // "top-left, bot-left, vline, line"
    
// MODE_BOX-specific 
jlog.mode_box_bg_chars= JXLOG_MODE_BOX_BG_CHARS; // The range where the background color is built on ;    
jlog.mode_box_border_colors= JXLOG_MODE_BOX_BORDER_COLORS; // border color choices;  


jlog.update= function(trg){for (var i=1;x=arguments[i];i++)
                           {for(var k in x)trg[k]=x[k]};
                          return trg 
            }    
jlog.repeat= function(str, n){ var s ='';
    for( var i=0;i<n;i++){ s+=str;}  return s;
}

    
jlog.getprefix= function(layer){// Return a string (for MODE_LINE) or arr(for MODE_BOX)
                                // ==> "|  |  " 
                                // ==> ["%c|  %c|  ", style1, style2] 
                                    
    if(typeof layer==='undefined') layer = this.blocks.length;
    
    if( layer==0 || this.mode == this.MODE_GROUP) {  // MODE_GROUP 
    
        var s=''
        
    }else
    
    if( this.mode == this.MODE_BOX ){  // MODE_BOX
        
        for( var i=0, s='', block; i<layer; i++ ){
            s += '%c'+this.indent
        }
    
        for( var i=0, s=[s], block; i<layer; i++ ){
            s.push( this.blocks[i].style )          //["%c|  %c!  ", style0, style1]          
        }
    
    } else {   // MODE_LINE
    
        for( var i=0, s='', block; i<layer; i++ ){
            s += this.blocks[i].symbol_vline+this.indent
        }
    }        
    return s
}
 
// MODE_BOX-specific

jlog.randombg=function(){   // random background color 
    var s='#',i=6;
    while(i--){
        s+= this.mode_box_bg_chars.charAt( parseInt(Math.random()*this.mode_box_bg_chars.length) )
    }
    return s
}
    
jlog.randombordercolor=function(){
    var c=this.mode_box_border_colors;
    return c[ parseInt(Math.random()*c.length) ]
}
 

jlog.addnewblock= function( ){

    //log_('in addnewblock, mode = ', this.mode )
    var layer = this.blocks.length;
    
    if( this.mode == this.MODE_LINE ){ 
    
        var symbs = this.mode_line_symbols;
        symbs = symbs[ layer % symbs.length ]  
          // symbs="top-left, bot-left, vline, line"
          
        var block=  { 
              symbol_vline   : symbs.charAt(2)
            , symbol_line    : this.repeat(symbs.charAt(3), 50)
            , symbol_corner0 : symbs.charAt(0)
            , symbol_corner1 : symbs.charAt(1)
        }
        
    } else 
    
    if( this.mode == this.MODE_BOX ){ 
        
        if( this.blockstyles.length> layer )
        {
            var block= { style: this.blockstyles[layer] 
                       }
            
        }else{
        
            var bg = this.randombg();
            var brd= this.randombordercolor() ;
            var block={ style: 'border-color:'+brd
                              +';border-style: solid'
                              +';border-width: 0px 0px 0px 1px'
                              +';background:'+ bg
                      }
                      
            this.blockstyles.push( block.style ); 
            //log_('New blockstyle added, blockstyle= ', jx.inspect( this.blockstyles ) );
        }
    }
    
    else { var block = {} } // MODE_GROUP
    
    //###################################
    //
    //  Adding .index to each block. 
    //
    //  .index increases when blocks on the same layer increase.
    //
    //  It is controlled by a ".subindex" var of their parent block
    //
    //  So each block now will have two new vars: .subindex, .index
    //
    //  We also throw in a .parent for easier op for parent.subindex
    //  If it's a top-layer (layer==0), parent = jlog
    //
    block.subindex=0;
    block.parent = layer==0?this: this.blocks[ this.blocks.length-1 ];
    block.index  = block.parent.subindex    
    block.parent.subindex ++
    //
    //  End adding .subcount, .index
    //###############################
    this.blocks.push( block )
          
}
  
jlog.b= function(){ 

    //log_('Enter jlog.b, mode = ', this.mode )
    
    this.isbeg=true;
    var args= [].slice.apply(arguments);
    
    if(args.length==0){
    
        if( this.show_func_args ){
          args= [ arguments.callee.caller.name+'(', arguments.callee.caller.arguments, ')' ];
        } else {
          args= [ arguments.callee.caller.name+'()' ];
        }
    }

    this.addnewblock(); 
    
    //log_('in jlog.b b4 apply, this.blocks = ', jx.inspect(this.blocks) )
    jlog.apply(null, args);  
    this.isbeg=false;
}
 
jlog.b0 = function(){
    this.resetindex();
    this.b.apply( this, [].slice.apply( arguments ) );
}         
         
jlog.e= function(){ //console.log('jlog.e')
                    this.isend=true;
                    this.apply(null, arguments);  
                    this.blocks.pop();
                    this.isend=false;
                  }; 
                  
    

jlog.resetindex= function(){
    var parent = this.blocks.length==0? this:this.blocks[ this.blocks.length-1];
    parent.subindex=0;

    this.blockstyles[ this.blocks.length]=  
               'border-color:'+ this.randombordercolor()
              +';border-style: solid'
              +';border-width: 0px 0px 0px 1px'
              +';background:'+ this.randombg()
    
}
 
jlog.__history__ ={
     '130915.2':['New functions: .resetindex() and jlog.b0(). When index reset, style, too']
    ,'130915.1':['Each block has a .subindex and .index. Show .index in beg and end']
    ,'130914.3':['A little code cleaning.']
    ,'130914.2':['All bugs fixed. jlog now is as good as jxlog.']
    ,'130914.1':['MODE_GROUP done. Fixing MODE_BOX: box top/bottom line didn\'t show.'
                 ,'http://jsfiddle.net/runsun/W4Mfs/15/'     ]
    ,'130913.1':['Restructuring: new design: jlog. MODE_LINE done.'
                 ,'http://jsfiddle.net/runsun/W4Mfs/14/'     ]
    ,'130911.1':['MODE_LINE done by patching up. Need code cleanup. TO DO: restructuring'
                 ,'http://jsfiddle.net/runsun/W4Mfs/14/'     ]
    ,'130910.1':['Add new mode: MODE_LINE (need fixes on log.e)'  ]
    ,'130806.1':['Set jxlog.on=true/false, or jxlog(true/false) to enable/disable logging.'  ]
    ,'130805.3':['Use jxlog.blockbgs to keep blocks of same layer the samg bg. '  ]
    ,'130805.2':['Add new options: block_beg, block_end'
               , 'Allow other formatting symbol: %o, %i, %f, %s besides %i and %O'  ]
    ,'130805.1':['Allows only MODE_BOX and MODE_GROUP'
               , 'Make MODE_BOX the default.',  
               , 'Add new option show_func_args, init_string.']
    ,'130804.2':['Redesign large part to allow MODE_BOX, which works nicely.']
    ,'130804.1':['Use .prefixes (an array) in place of _getprefix()'
               ,'Allow setting modes: jxlog.MODE=0 (default) is the plain mode'
               +', jxlog.MODE=jxlog.MODE_GROUP uses the console group()']
    ,'130803.1':['first draft']
    }


