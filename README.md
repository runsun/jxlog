jxlog.js
========

A javascript logging tool allows for **layered display of nested blocks** in the console. 

### Features

  * Define block begin and end with `jxlog.b(...)` and `jxlog.e(...)`, respectively
  * `jxlog.b()` automatically logs function name (and/or arguments) to begin a block
  * Depending on `jxlog.mode`, each block could show up in 'block', 'lines' or 'group'
  * Every children block is indented to display nested blocks 
  * Each block has an *index*, which increases within the same layer;
  * An index of a block can be reset to 0 using `jxlog.b0()`.  
  * When in *jxlog.MODE_BOX* mode, blocks show up in random colors. 


    ![](https://lh5.googleusercontent.com/-kqsSEQVKQQ0/UjZrnH-DmsI/AAAAAAAAERQ/X6T07t75kzw/w366-h450-no/Screenshot_jxlog_chrome_mode_box.png)


### Brief api

    jxlog.b(x,y,z) - log x,y,z as the beginning of a block
    jxlog.b()      - if placed inside a function, automatically log function name to begin a block
    jxlog(x,y,z)   - log x,y,z
    jxlog.e(x,y,z) - log x,y,z as the end of a block
    jxlog.e()      - If no arg given, show the block header displayed by its corresponding `jxlog.b(header)`

    jxlog.b0(x,y,z) - log x,y,z as the beginning of a block with block index reset to 0


---------------------------
### Options

Set different modes:

    jxlog.mode = jxlog.MODE_BOX // this is the default
    jxlog.mode = jxlog.MODE_LINE
    jxlog.mode = jxlog.MODE_GROUP

Other options:

    jxlog.indent   = "  ";
    jxlog.show_func_args= false;  // Show function arguments when using jxlog.b(). If true, function 
                                  // block will starts with funcname( arguments ) in stead of funcname()  

See each mode section for mode-specific options.

---------------

### Usage

Simply add the following line to your doc:

    <script>(your path here).../jxlog.js</script>

then use it like what's shown below. You can play with the following code in [jsfiddle](http://jsfiddle.net/runsun/W4Mfs/20/).

```
#!javascript

var l= jxlog;
//l.mode= l.MODE_LINE
//l.mode= l.MODE_GROUP
//l.SHOW_FUNC_ARGS= true;

l('start test') // jxlog() alone works like console.log()
function ff(a){
    l.b()       // .b(): begin a block. Ii will get function
                // name automatically if no arg given. If
                // jxlog.show_func_args is true, it tries to
                // get function args automatically.
    n=3
    l('start with n=', a) // normal log line
    
    l.b('while')          // Start another block with given arg    
    while(n--){
    
       l('n=', n)           // normal log line
       
       l.b('while n=',n)    // block
       l('test array display:', [n,1], ' obj:', {a:3})  // test object 
       l('nnn')   
       l.e('Ended while')   // ending  a block 
       
    }  
    
    l('Block index reset by using .b0():')
    // Using jxlog.b0() to start a new block with reset index (=0)
    
    l.b0('test index')      // index-0 block
    l('inside test index')
    l.e()

    l.b('test index2')
    l('inside test index2')
    l.e()
        
    l.e() 
 l.e()   
}

ff(3, 2)    
```

--------------------
### Modes

There are 3 modes of operations, producing 3 different formats

--------------------
**MODE_BOX**

This is achieved with:

```
jxlog.mode = jxlog.MODE_BOX
```

In this *default* mode, blocks are formatted with border and background colors. Key features:

* Block border and background colors are randomly selected from predetermined parameters:
* New random colors are used for blocks added as a new layer;
* Same colors are used for blocks added as the same layer of previous one, except when the block is started with `jxlog.b0()`, which resets *index* and changes colors;

The background color is constructed by picking a char from *JXLOG_MODE_BOX_BG_CHARS* randomly 6 times. The border color is constructed by randomly pick a setting from *JXLOG_MODE_BOX_BORDER_COLORS*. Default values of these two parameters are: 

    jxlog.mode_box_bg_chars="bcdef" // The range where the background color is built on ; 

Allowed color ranges is `123456789abcdef`. If you prefer darker colors, pick chars closer to the left.

    jxlog.mode_box_border_colors= 
      ["green", "darkred", "blue", "black", "purple"] // border color random choices;


Two examples of mode_box (chrome on Linux Mint): 


  ![](https://lh6.googleusercontent.com/-jBqphb51-nE/UjZrnNReVnI/AAAAAAAAERI/pRaZ5OS5zF8/w381-h453-no/Screenshot_jxlog_chrome_mode_box2.png) ![](https://lh5.googleusercontent.com/-kqsSEQVKQQ0/UjZrnH-DmsI/AAAAAAAAERQ/X6T07t75kzw/w366-h450-no/Screenshot_jxlog_chrome_mode_box.png)

-------------------

**MODE_LINE**

Extra lines are used to build block borders. MODE_LINE-specific option:

    jxlog.mode_line_symbols= ["++|=", "oo|-", "++|=", "oo|*"] // "top-left, bot-left, vline, line"

'MODE_LINE' in Chrome:


   ![](https://lh5.googleusercontent.com/-SFhULZ3YpME/UjZrnrQFdmI/AAAAAAAAERc/jKMVOuAaLts/w412-h564-no/Screenshot_jxlog_chrome_mode_line.png)


'MODE_LINE' in Firefox:


   ![](https://lh4.googleusercontent.com/-tOfOlBmkg4w/UjZrn2ZSZBI/AAAAAAAAERk/WNQJKoViTAI/w623-h400-no/Screenshot_jxlog_firefox_mode_line.png)



-------------------

**MODE_GROUP**

'MODE_GROUP' makes use of browser-provided collapsible feature.

'MODE_GROUP' in Chrome:


   ![](https://lh5.googleusercontent.com/-ZUpWr7EE5Ws/UjZrnIKZdtI/AAAAAAAAERE/vjMGTadczDI/w397-h337-no/Screenshot_jxlog_chrome_mode_group.png)


'MODE_GROUP' in Firefox:


   ![](https://lh4.googleusercontent.com/-Q4EUCmO5_ms/UjZrnwjPmOI/AAAAAAAAERg/9P7IRm_tebM/w602-h419-no/Screenshot_jxlog_firefox_mode_group.png)



-----------------
### Ref 

* [Chrome console-api](https://developers.google.com/chrome-developer-tools/docs/console-api)
* [Chrome commandline-api](https://developers.google.com/chrome-developer-tools/docs/commandline-api)
* [Firefox](http://getfirebug.com/logging)    